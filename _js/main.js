;(function () {

    //Portfolio popup click handler//
    $('#bannerAside, #eq_image, #partners_certificate').magnificPopup({
	    type: 'image',
	    removalDelay: 300,
        mainClass: 'mfp-fade'
	});

	//Маска для ввода телефона
	$('[name=phone]').inputmask("+7 (999) 999 99 99");
	$('[name=email]').inputmask("*{1,64}@*{1,64}[.a{1,3}]");

	//Сдвиг меню вверх
	function scrollDesktop() {

		$(window).scroll(function() {
			//NY - >= 100
			if ($(this).scrollTop() >= 50) {
				$('.main-navbar').addClass('main-navbar--fixedtop');
				$('.nav').addClass('nav--fixedtop');
			}
			else {
				$('.main-navbar').removeClass('main-navbar--fixedtop');
				$('.nav').removeClass('nav--fixedtop');
			}
		});
	}

	/*=====================================
	=            Print Element            =
	=====================================*/

	var PHE = printHtmlElement;

	// document.getElementById('printStrIframeButton').addEventListener('click', function(e){
 //        e.preventDefault();
 //        printHtmlElement.printHtml("<h1>Let's print this h1</h1><p>...and while we're at it, this p as well will do nicely.</p>", {pageTitle: 'From HTML String', printMode: 'iframe'});
 //    });

	$('#printButton').on('click', function(e) {
		e.preventDefault();
		PHE.printHtml("<div><img src=\"assets/images/sidebar/BDC_cupon_print.jpg\" alt=\"Купон на скидку\"></div>", {
				printMode: 'iframe'
			});
	});

	/*=====  End of Print Element  ======*/

	/*===================================
    =            Menu Mobile            =
    ===================================*/

    var navToggleMobile = $('#navToggleMobile'),
        navToggleLine = $('.mobile-line'),
        navToggleLineBefore = $('.mobile-line--before'),
        navToggleLineAfter = $('.mobile-line--after'),
        navMenuMobile = $('#navMainMobile'),
        navContainer = $('.container'),
        navMenuMobileLi = $('#navMainMobile ul > li'),
        navMenuAdress = $('.nav--mobile__contact')
    ;

    function menuLeftMobile() {

        var tl = new TimelineMax({
        	paused: true,
            reversed: true
        });

        tl
            .to(navMenuMobile, 0.4, {
                top: 0,
                // zIndex: 9998,
                ease: Power1.easeInOut
            }, "-0.8")
            .staggerFrom(navMenuMobileLi, 0.4, {
                yPercent: "-50",
                autoAlpha: 0,
                ease: Back.easeInOut
            }, 0.1)
            .from(navMenuAdress, 0.4, {
                // yPercent: "-50%",
                autoAlpha: 0,
                ease: Back.easeInOut
            }, "-=0.3")
            .set(navToggleMobile, {
                className: "+=nav-toggle--open"
            })
            // .to(navToggleLine, 0.3, {
            //     height: "0",
            //     ease: Power1.easeInOut},
            //     "-=0.6")
            // .to(navToggleLineBefore, 0.5, {
            //     rotation: "45",
            //     y: "0",
            //     ease: Power2.easeInOut},
            //     "-=0.6")
            // .to(navToggleLineAfter, 0.5, {
            //     rotation: "-45",
            //     y: "0",
            //     ease: Power2.easeInOut},
            //     "-=0.6")
        ;

        navToggleMobile.click(function () {
            tl.reversed() ? tl.restart() : tl.reverse("+0.6");
        });
        // navContainer.click(function () {
        //     tl.reverse();
        // });

        return tl;
    }

    /*=====  End of Menu Mobile  ======*/

    /*=================================================
    =            Yandex Map Contact Mobile            =
    =================================================*/

    var contactMapLink_1 = $('#yandexMap_0'),
        contactMapLink_2 = $('#yandexMap_1'),
        contactMapBlock_1 = $('#yandexMapBlock_0'),
        contactMapBlock_2 = $('#yandexMapBlock_1'),
        contactNavMobile = $('.main-navbar, .footer'),
        contactMapClose_1 = $('#contactCloseMobile_0'),
        contactMapClose_2 = $('#contactCloseMobile_1')
    ;

    function contactMapMobile_1() {

        var tl = new TimelineMax({
            paused: true,
            reversed: true
        });

        tl
            .to(contactMapBlock_1, 0.4, {
                yPercent: "100",
                autoAlpha: 1,
                // zIndex: 9998,
                ease: Sine.easeInOut
            })
            .to(contactNavMobile, 0.4, {
                // yPercent: "-100",
                autoAlpha: 0,
                zIndex: -1,
                ease: Sine.easeInOut
            }, "-=0.2")
            .to(contactMapClose_1, 0.4, {
                yPercent: "100",
                autoAlpha: 1,
                // zIndex: 9998,
                ease: Sine.easeInOut
            }, "-=0.2")
        ;

        contactMapLink_1.click(function () {
            tl.reversed() ? tl.restart() : tl.reverse();
        });
        contactMapClose_1.click(function () {
            tl.reverse();
        });

        return tl;
    }

    function contactMapMobile_2() {

        var tl = new TimelineMax({
            paused: true,
            reversed: true
        });

        tl
            .to(contactMapBlock_2, 0.4, {
                yPercent: "100",
                autoAlpha: 1,
                // zIndex: 9998,
                ease: Sine.easeInOut
            })
            .to(contactNavMobile, 0.4, {
                // yPercent: "-100",
                autoAlpha: 0,
                zIndex: -1,
                ease: Sine.easeInOut
            }, "-=0.2")
            .to(contactMapClose_2, 0.4, {
                yPercent: "100",
                autoAlpha: 1,
                // zIndex: 9998,
                ease: Sine.easeInOut
            }, "-=0.2")
        ;

        contactMapLink_2.click(function () {
            tl.reversed() ? tl.restart() : tl.reverse();
        });
        contactMapClose_2.click(function () {
            tl.reverse();
        });

        return tl;
    }

    /*=====  End of Yandex Map Contact Mobile  ======*/


    /*=====================================
    =            Init Function            =
    =====================================*/

    function init() {
    	scrollDesktop();
    }

    function initMobile() {
    	menuLeftMobile();
        contactMapMobile_1();
        contactMapMobile_2();
    }

    if (document.body.clientWidth > 420 || screen.width > 420) {

        window.onload = function() {
            init();
        };

    } else {

        window.onload = function() {
            initMobile();
        };
    }

    /*=====  End of Init Function  ======*/


}(jQuery));
